# TrafficSigns
В рамках данного проекта выполнялось обучение модели YOLO на задаче детектирования дорожных знаков на наборе данных [Russian Traffic Signs Dataset](https://www.kaggle.com/datasets/watchman/rtsd-dataset) с помощью инструмента [Ultralytics](https://github.com/ultralytics/ultralytics). Протестировать работу модели можно с помощью Telegram бота [TrafficSignBot](https://t.me/trafficsignbot)

## Зависимости
- [x] [CUDA 11.8](https://developer.nvidia.com/cuda-11-8-0-download-archive?target_os=Linux)
- [x] [cuDNN 8.9](https://developer.nvidia.com/downloads/compute/cudnn/secure/8.9.4/local_installers/11.x/cudnn-local-repo-ubuntu2004-8.9.4.25_1.0-1_amd64.deb/)
- [x] [Git](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04)
- [x] [Python 3.10](https://computingforgeeks.com/how-to-install-python-on-ubuntu-linux-system/)
- [x] [Python3-pip](https://linuxize.com/post/how-to-install-pip-on-ubuntu-20.04/)
- [x] [Python3-venv](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-programming-environment-on-ubuntu-20-04-quickstart)

## Быстрый старт на Ubuntu
1. Создайте виртуальное окружение с помощью Python-venv или Anaconda.
    ```
    python3.10 -m venv venv
    source venv/bin/activate
    ```

    ```
    conda create -n venv python=3.10
    source activate venv
    ```

2. Установите менеджер пакетов Poetry.
    ```
    pip install poetry
    ```

3. Установите все необходимые зависимости проекта.
    ```
    poetry install --no-root
    ```

4. Соберите локальный пакет **src** с помощью команды:
    ```
    python setup.py build install
    ```

5. Скачайте набор данных [Russian Traffic Signs Dataset]() в директорию **/datasets**. Необходимо выберать первую версию датасета, т.к. она содержит файл **full-gt.csv**. В рамках данного проекта обработка данных выполнялась с его помощью.

6. Выполните скрипт с обработкой датасета. На данном этапе разметка изображений преобразуется в формат, подходящий для обучения YOLO, а также проводится разбиение на тренировочную и валидационную выборки.

    ```bash
    python scripts/prepare_data.py --data_path <путь_к_full-gt.csv> --width <целевая_ширина_кадра> --height <целевая_высота_кадра> --train_ratio <доля_тренировочных_данных>
    ```

7. Создайте файл **hyp.scratch.yaml**, содержащий гиперпараметры обучения и вероятности аугментации. Примерное содержание файла:
    ```yaml
    lr0: 0.001  # initial learning rate (SGD=1E-2, Adam=1E-3)
    lrf: 0.2  # final OneCycleLR learning rate (lr0 * lrf)
    momentum: 0.937  # SGD momentum/Adam beta1
    weight_decay: 0.0005  # optimizer weight decay 5e-4
    warmup_epochs: 3.0  # warmup epochs (fractions ok)
    warmup_momentum: 0.8  # warmup initial momentum
    warmup_bias_lr: 0.1  # warmup initial bias lr
    box: 0.05  # box loss gain
    cls: 0.5  # cls loss gain
    cls_pw: 1.0  # cls BCELoss positive_weight
    obj: 1.0  # obj loss gain (scale with pixels)
    obj_pw: 1.0  # obj BCELoss positive_weight
    iou_t: 0.20  # IoU training threshold
    anchor_t: 4.0  # anchor-multiple threshold
    # anchors: 3  # anchors per output layer (0 to ignore)
    fl_gamma: 0.0  # focal loss gamma (efficientDet default gamma=1.5)
    hsv_h: 0.015  # image HSV-Hue augmentation (fraction)
    hsv_s: 0.7  # image HSV-Saturation augmentation (fraction)
    hsv_v: 0.4  # image HSV-Value augmentation (fraction)
    degrees: 0.0  # image rotation (+/- deg)
    translate: 0.1  # image translation (+/- fraction)
    scale: 0.5  # image scale (+/- gain)
    shear: 0.0  # image shear (+/- deg)
    perspective: 0.001  # image perspective (+/- fraction), range 0-0.001
    flipud: 0.0  # image flip up-down (probability)
    fliplr: 0.0  # image flip left-right (probability)
    mosaic: 1.0  # image mosaic (probability)
    mixup: 0.0  # image mixup (probability)
    ```

8. Укажите пути к тренировочной и валидационной выборке в файле [rstd-frames.yaml](datasets/rstd-frames.yaml).

9. Обучите модель с помощью следующей команды:
    ```bash
    python scripts/train.py --dataset rtsd-frames --epochs <кол-во эпох>
    ```

10. В дирктории **/runs/detect** будут храниться результаты всех экспериментов обучения. В папке эксперимента содержится папка **weights**, где хранится последний и лучший чекпоинт модели. По результатам обучения скопируйте файл **best.pt** в директорию [models/](models/) и переименуйте в **yolov8n-rtsd.pt**.

11. Если вы хотите встроить модель в ваш Telegram-бот, то укажите в файле [scripts/bot.py](scripts/bot.py) токен вашего бота.
    ```python
    TOKEN = "YOUR-TOKEN"
    ```

12. Запутстите бота на вашем компьютере или сервере
    ```
    python scripts/bot.py
    ```

## Результаты обучения
В рамках данного проекта было проведено обучение модели на 50 эпохах, гиперпараметры указаны в п.7 Быстрого старта. Приведу некоторые результаты обучения.

![Plots](reports/imgs/results.png)
*Графики изменения метрик во время обучения*

![PR](reports/imgs/PR_curve.png)
*PR-кривая по всем классам*

![PC](reports/imgs/P_curve.png)
*PC-кривая по всем классам*

![RC](reports/imgs/R_curve.png)
*RC-кривая по всем классам*

![F1C](reports/imgs/F1_curve.png)
*F1C-кривая по всем классам*

![CM](reports/imgs/confusion_matrix_normalized.png)
*Матрица ошибок*

| train/box_loss | train/cls_loss | train/dfl_loss | val/box_loss | val/cls_loss | val/dfl_loss | precision | recall | mAP50 | mAP50-95|
|----|----|----|----|----|----|----|----|----|----|
|0.928|0.917|0.832|0.881|0.647|0.824|0.658|0.570|0.644|0.465|

*Некоторые метрики, полученные по результатам обучения*

## Тестирование бота
Запустите бота в Телеграме, нажав на **Старт**. Если ваш исходный код верный, то бот ответит приветствием.

![Start](reports/imgs/start.png)
![Greeting](reports/imgs/greeting.png)

Отправьте изображение боту. Обратите внимание, что отправить фото нужно именно картинкой, а не документом. Если всё верно, то бот отправит в ответ обработанное изображение.

![Result](reports/imgs/test.png)
