import click
import shutil
from os import path
from pathlib import Path
from torch.utils.data import random_split
from torchvision.transforms import Compose
from tqdm import tqdm

from src.data import TrafficSignsDataset
from src.transform import Resize, Normalize, ToTensor
from src.utils import ROOT

@click.command()
@click.option(
    '--data_path',
    type=click.Path(exists=True),
    default=Path("../data/rtsd-frames/"),
    help="Path to .csv file with dataset information"
)
@click.option(
    '--width',
    type=click.INT,
    default=1024,
    help="Target width of images."
)
@click.option(
    '--height',
    type=click.INT,
    default=1024,
    help="Target height of images."
)
@click.option(
    '--train_ratio',
    type=click.FLOAT,
    default=0.8,
    help="Ratio of training data in total dataset."
)
def prepare_data(data_path: Path, width: int, height: int, train_ratio: float):
    csv_path = data_path.joinpath("full-gt.csv")
    transform = Compose([Resize((width, height)),
                         ToTensor(),
                         Normalize()])
    dataset = TrafficSignsDataset(str(csv_path), transform)
    train_size = int(train_ratio * len(dataset))
    test_size = len(dataset) - train_size

    train_dataset, test_dataset = random_split(dataset, [train_size, test_size])

    train_save_path = str(data_path.joinpath("train"))
    valid_save_path = str(data_path.joinpath("valid"))

    for data in tqdm(train_dataset):
        if data:
            name = Path(data['img_path']).stem
            label_path = f"{train_save_path}/labels/{name}.txt"
            image_path = f"{train_save_path}/images/{name}.jpg"
            with open(label_path, 'w') as f:
                f.write(f"{data['label']} {data['bbox'][0]} {data['bbox'][1]} {data['bbox'][2]} {data['bbox'][3]}")
            
            shutil.move(data['img_path'], image_path)

    for data in tqdm(test_dataset):
        if data:
            name = Path(data['img_path']).stem
            label_path = f"{valid_save_path}/labels/{name}.txt"
            image_path = f"{valid_save_path}/images/{name}.jpg"
            with open(label_path, 'w') as f:
                f.write(f"{data['label']} {data['bbox'][0]} {data['bbox'][1]} {data['bbox'][2]} {data['bbox'][3]}")
            
            shutil.move(data['img_path'], image_path)
        

if __name__ == "__main__":
    prepare_data()





