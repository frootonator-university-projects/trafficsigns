import click
from src.utils import ROOT
from ultralytics import YOLO

@click.command()
@click.option(
    "--dataset",
    type=click.STRING,
    default="rstd-frames",
    help="Name of training dataset."
)
@click.option(
    "--epochs",
    type=click.INT,
    default=70,
    help="Number of epochs."
)
def main(dataset: str, epochs: int):
    data_path = f"{ROOT}/datasets/{dataset}.yaml"
    model = YOLO()
    results = model.train(resume=True)


if __name__ == "__main__":
    main()
