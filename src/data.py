import cv2
import pandas as pd
import torch
from os import path
from pathlib import Path
from PIL import Image
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import Dataset
from .utils import ROOT


DATA_PATH = ROOT.joinpath('data/rtsd-frames/rtsd-frames')

class TrafficSignsDataset(Dataset):
    def __init__(self, csv_file: str, transform=None):
        data = pd.read_csv(csv_file)
        encoder = LabelEncoder()

        self.img_path = data['filename'].apply(lambda x: str(DATA_PATH.joinpath(x))).values
        self.x = data['x_from'].values
        self.y = data['y_from'].values
        self.w = data['width'].values
        self.h = data['height'].values
        
        data['sign_class'] = encoder.fit_transform(data['sign_class'])
        self.label = data['sign_class'].values

        self.transform = transform

    def __len__(self):
        return len(self.img_path)
    
    def __getitem__(self, idx):
        if path.exists(self.img_path[idx]):
            img = cv2.imread(self.img_path[idx])
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            bbox = [self.x[idx],
                    self.y[idx],
                    self.w[idx],
                    self.h[idx]]
            
            label = self.label[idx]

            sample = {'img_path': self.img_path[idx],
                    'img': img,
                    'bbox': bbox,
                    'label': label}
            
            if self.transform:
                sample = self.transform(sample)
            
            return sample
        
        else:
            return None