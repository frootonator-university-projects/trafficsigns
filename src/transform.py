import cv2
import numpy as np
import torch

class Resize(object):
    def __init__(self, dst_size: tuple):
        self.dst_size = dst_size
    def __call__(self, sample):
        src_size = sample['img'].shape
        sample['img'] = cv2.resize(sample['img'],
                                   self.dst_size)
        
        y_scale = float(self.dst_size[0] / src_size[0])
        x_scale = float(self.dst_size[1] / src_size[1])

        sample['bbox'][0] = int(x_scale * sample['bbox'][0])
        sample['bbox'][1] = int(y_scale * sample['bbox'][1])
        sample['bbox'][2] = int(x_scale * sample['bbox'][2])
        sample['bbox'][3] = int(y_scale * sample['bbox'][3])

        return sample
    

class Normalize(object):
    def __init__(self, mean=[127.5, 127.5, 127.5], std=[127.5, 127.5, 127.5]):
        self.mean = torch.as_tensor(mean, dtype=torch.float32)
        self.std = torch.as_tensor(std, dtype=torch.float32)

    def __call__(self, sample):
        sample['img'] = (sample['img'] - self.mean) / self.std

        height, width, _ = sample['img'].shape

        sample['bbox'][0] = (sample['bbox'][0] + sample['bbox'][2] / 2) / width
        sample['bbox'][1] = (sample['bbox'][1] + sample['bbox'][3] / 2) / height
        sample['bbox'][2] = sample['bbox'][2] / width
        sample['bbox'][3] = sample['bbox'][3] / height
        
        return sample


class ToTensor(object):
    def __call__(self, sample):
        sample['img'] = torch.as_tensor(sample['img'], dtype=torch.float32)
        sample['bbox'] = torch.as_tensor(sample['bbox'], dtype=torch.float32)
        sample['label'] = torch.as_tensor(sample['label'], dtype=torch.int64)
        
        return sample